class RegionsController < ApplicationController
  def show
    @region = Region.find(params[:id])
    @dogs = Dog.where(region_id: params[:id])
  end
end
