class ChangeDogs < ActiveRecord::Migration[5.0]
  def change
    remove_column :dogs, :region
    add_reference :dogs, :region, index: true
  end
end
