class CreateDogs < ActiveRecord::Migration[5.0]
  def change
    create_table :dogs do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.string :region, null: false
      t.integer :ns_id, null: false
      t.string :location, null: false
    end
  end
end
